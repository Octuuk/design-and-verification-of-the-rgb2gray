class mysequence extends uvm_sequence#(mytransaction);
  
  `uvm_object_utils(mysequence)
  mytransaction packet;
  function new(string name="mysequence");
    super.new(name);
    packet=mytransaction::type_id::create("packet");
  endfunction
  
  task body();
    repeat(100000)begin
      start_item(packet);
      packet.randomize();
      finish_item(packet);
    end
  endtask
  
endclass