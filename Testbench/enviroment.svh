class myenv extends uvm_env;
  
  `uvm_component_utils(myenv)
  myagent agt0;
  myscoreboard scb0;
  
  function new(string name="myenv",uvm_component parent=null);
    super.new(name,parent);
  endfunction
  
  function void build_phase(uvm_phase phase);
    super.build_phase(phase);
    agt0=myagent::type_id::create("agt0",this);
    scb0=myscoreboard::type_id::create("scb0",this);
  endfunction
  
  function void connect_phase(uvm_phase phase);
    agt0.mon0.monap.connect(scb0.scbimp);
  endfunction
  
endclass