class mymonitor extends uvm_monitor;
  
  virtual axi_stream monif;
  mytransaction monpacket;
  uvm_analysis_port #(mytransaction) monap;
  `uvm_component_utils(mymonitor)
  
  reg pix_one=1'b0;
  reg pix_two=1'b0;
  
  reg pix_one_output=1'b0;
  reg pix_two_output=1'b0;
  
  function new(string name="mymonitor",uvm_component parent=null);
    super.new(name,parent);
  endfunction
  
  function void build_phase(uvm_phase phase);
    super.build_phase(phase);
    monpacket=mytransaction::type_id::create("monpacket");
    uvm_config_db#(virtual axi_stream)::get(null,"tbtop","tbif",monif);
    monap=new("monap",this);
  endfunction
  
  task run_phase(uvm_phase phase);
      @(posedge monif.axi_aclk);
        monpacket.s_axis_tdata  = monif.s_axis_tdata;
        monpacket.s_axis_tvalid = monif.s_axis_tvalid;
        monpacket.s_axis_tready = monif.s_axis_tready;
        monpacket.s_axis_tlast  = monif.s_axis_tlast;
        monpacket.s_axis_tuser  = monif.s_axis_tuser; 
        pix_one = 1'b1; // we collected first pixel input to the module.
      forever begin
      
        @(posedge monif.axi_aclk);
        monpacket.m_axis_tdata  = monif.m_axis_tdata;
        monpacket.m_axis_tvalid = monif.m_axis_tvalid;
        monpacket.m_axis_tready = monif.m_axis_tready;
        monpacket.m_axis_tlast  = monif.m_axis_tlast;
        monpacket.m_axis_tuser  = monif.m_axis_tuser;
        pix_one_output = 1'b1; // pix one and its output captured.Now drive it to the write function
        
        pix_one = 1'b0;
        pix_one_output = 1'b0;
        //monpacket.print();
        monap.write(monpacket); // after this collect pix two from s_axis
        
        monpacket.s_axis_tdata  = monif.s_axis_tdata;
        monpacket.s_axis_tvalid = monif.s_axis_tvalid;
        monpacket.s_axis_tready = monif.s_axis_tready;
        monpacket.s_axis_tlast  = monif.s_axis_tlast;
        monpacket.s_axis_tuser  = monif.s_axis_tuser; 
        pix_two = 1'b1; // we collected first pixel input to the module.
        
        @(posedge monif.axi_aclk);
        monpacket.m_axis_tdata  = monif.m_axis_tdata;
        monpacket.m_axis_tvalid = monif.m_axis_tvalid;
        monpacket.m_axis_tready = monif.m_axis_tready;
        monpacket.m_axis_tlast  = monif.m_axis_tlast;
        monpacket.m_axis_tuser  = monif.m_axis_tuser; 
        pix_two_output = 1'b1; // we collected first pixel input to the module.
        //monpacket.print();
        monap.write(monpacket); // after this collect pix one from s_axis
        
        monpacket.s_axis_tdata  = monif.s_axis_tdata;
        monpacket.s_axis_tvalid = monif.s_axis_tvalid;
        monpacket.s_axis_tready = monif.s_axis_tready;
        monpacket.s_axis_tlast  = monif.s_axis_tlast;
        monpacket.s_axis_tuser  = monif.s_axis_tuser; 
        pix_one = 1'b1; // we collected first pixel input to the module.
      end // if end
  endtask
  
  
endclass