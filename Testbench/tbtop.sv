// Code your testbench here
// or browse Examples
`include "package.svh"
`include "uvm_macros.svh"
//`include "design.sv"
module tbtop;
  import mypackage::*;
  import uvm_pkg::*;
  axi_stream tbif();
  
  Rgb2Gray_V2	dut(
  // global signals
    .axi_aclk(tbif.axi_aclk),
    .axi_arstn(tbif.axi_arstn),
  // testbench master
    .s_axis_tdata(tbif.s_axis_tdata),
    .s_axis_tvalid(tbif.s_axis_tvalid),
    .s_axis_tready(tbif.s_axis_tready),//----
    .s_axis_tlast(tbif.s_axis_tlast),
    .s_axis_tuser(tbif.s_axis_tuser),
  // testbench slave
    .m_axis_tdata(tbif.m_axis_tdata),
    .m_axis_tvalid(tbif.m_axis_tvalid),
    .m_axis_tready(tbif.m_axis_tready),//----
    .m_axis_tlast(tbif.m_axis_tlast),
    .m_axis_tuser(tbif.m_axis_tuser)
  
  );
  
  
  initial begin
    uvm_config_db#(virtual axi_stream)::set(null,"*","tbif",tbif);
    run_test("mytest");
  end
  
  initial begin
    tbif.axi_aclk = 1'b0;
    tbif.axi_arstn = 1'b1;
    //tbif.m_axis_tready  = 1'b1;
    //tbif.s_axis_tvalid = 1'b1;
    forever begin
       #5 tbif.axi_aclk = ~tbif.axi_aclk; 
    end
  end
  
  
  initial begin
    $dumpfile("dump.vcd");
    $dumpvars(0,tbtop);
  end
  
  
endmodule