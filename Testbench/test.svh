class mytest extends uvm_test;
  
  `uvm_component_utils(mytest)
  myenv env0;
  
  function new(string name="mytest",uvm_component parent=null);
    super.new(name,parent);
  endfunction
  
  function void build_phase(uvm_phase phase);
    super.build_phase(phase);
    env0=myenv::type_id::create("myenv",this);
  endfunction
  
  task run_phase(uvm_phase phase);
    phase.raise_objection(this);
    env0.agt0.seq0.start(env0.agt0.seqr0);
    phase.drop_objection(this);
  endtask
  
endclass