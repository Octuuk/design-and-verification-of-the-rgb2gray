class myscoreboard extends uvm_scoreboard;
  
  `uvm_component_utils(myscoreboard)
  
  mytransaction scbpacket;
  uvm_analysis_imp #(mytransaction,myscoreboard) scbimp;
  event expected_comp;
  reg s_axis_ok;
  reg m_axis_ok;
  reg [10:0] total=11'b0;
  reg [10:0] ok=11'b0;
  reg [10:0] err=11'b0;
  
  reg dataerr=1'b0;
  reg tlasterr=1'b0;
  reg tusererr=1'b0;

  reg [7:0] pix_one_val;
  reg [7:0] pix_two_val;  

  reg [23:0] expected_pix_one;
  reg pix_one_ok=1'b0;
  reg [23:0] pix_one;
  reg 		 pix_one_tvalid;
  reg 		 pix_one_tready;
  reg 		 pix_one_tlast;
  reg 		 pix_one_tuser;
  
  reg [23:0] expected_pix_two;
  reg pix_two_ok=1'b0;
  reg [23:0] pix_two;
  reg 		 pix_two_tvalid;
  reg 		 pix_two_tready;
  reg 		 pix_two_tlast;
  reg 		 pix_two_tuser;
  
  function new(string name="myscoreboard",uvm_component parent=null);
    super.new(name,parent);
  endfunction
  
  function void build_phase(uvm_phase phase);
    super.build_phase(phase);
    scbimp=new("scbimp",this);
  endfunction
  
  task write(mytransaction scbpacket);// now there are one clock latency between input and output so we need to take consideration it.
	
    // first compare if input data is valid or not. if it is then store it and compare it to the next valid output
    if(scbpacket.s_axis_tvalid && scbpacket.s_axis_tready && !pix_one_ok)begin
      pix_one        = scbpacket.s_axis_tdata;
      pix_one_tvalid = scbpacket.s_axis_tvalid;
      pix_one_tlast  = scbpacket.s_axis_tlast;
      pix_one_tuser  = scbpacket.s_axis_tuser;
      pix_one_ok = 1'b1;
      pix_one_val = {(pix_one[23:16]>>2) + (pix_one[23:16]>>5) + (pix_one[15:8]>>1) + (pix_one[15:8]>>4) + (pix_one[7:0]>>4) + (pix_one[7:0]>>5)};
      expected_pix_one ={pix_one_val,pix_one_val,pix_one_val};
      //expected_pix_one = (pix_one[23:16]>((pix_one[23:16]>>2) + (pix_one[23:16]>>5) + (pix_one[15:8]>>1) + (pix_one[15:8]>>4) + (pix_one[7:0]>>4) + (pix_one[7:0]>>5) + 6'd46))?24'hFFFFFF:24'h000000;
    end
    else if(scbpacket.s_axis_tvalid && scbpacket.s_axis_tready && !pix_two_ok)begin
      pix_two        = scbpacket.s_axis_tdata;
      pix_two_tvalid = scbpacket.s_axis_tvalid;
      pix_two_tlast  = scbpacket.s_axis_tlast;
      pix_two_tuser  = scbpacket.s_axis_tuser;
      pix_two_ok = 1'b1;
      pix_two_val = {(pix_two[23:16]>>2) + (pix_two[23:16]>>5) + (pix_two[15:8]>>1) + (pix_two[15:8]>>4) + (pix_two[7:0]>>4) + (pix_two[7:0]>>5)};
      expected_pix_two = {pix_two_val,pix_two_val,pix_two_val};
      //expected_pix_two = (pix_two[23:16]>((pix_one[23:16]>>2) + (pix_one[23:16]>>5) + (pix_one[15:8]>>1) + (pix_one[15:8]>>4) + (pix_one[7:0]>>4) + (pix_one[7:0]>>5) + 6'd46))?24'hFFFFFF:24'h000000;
    end
    else begin
      // no op
    end
    
    // depolama işlemi tamam.Şimdi karşılaştırma var.İlk önce giren ilk önce çıkmalı
    if(scbpacket.m_axis_tvalid && scbpacket.m_axis_tready && pix_one_ok)begin// modülden pixel çıktı.Şimdi karşılaştır.
      if(scbpacket.m_axis_tdata != expected_pix_one)begin
        $display("Data Err..Input=%0x Output=%0x Expected=%0x \n",pix_one,scbpacket.m_axis_tdata,expected_pix_one);
        dataerr = 1'b1;
        $finish();
      end
      if(scbpacket.m_axis_tlast != pix_one_tlast)begin
        $display("Tlast Err... Input=%0x Output=%0x Expected=%0x \n",pix_one_tlast,scbpacket.m_axis_tlast,pix_one_tlast);
        tlasterr = 1'b1;
        $finish();
      end
      if(scbpacket.m_axis_tuser != pix_one_tuser)begin
        $display("Tuser Err... Input=%0x Output=%0x Expected=%0x \n",pix_one_tuser,scbpacket.m_axis_tuser,pix_one_tuser);
        tusererr=1'b1;
        $finish();
      end
      if(dataerr | tlasterr | tusererr )begin
        $display("ERR.Abort... \n");
        $finish();
      end
      $display("Input=%0x output=%0x expected=%0x ------ OK ",pix_one,scbpacket.m_axis_tdata,expected_pix_one);
      pix_one_ok = 1'b0;
    end
    
    if(scbpacket.m_axis_tvalid && scbpacket.m_axis_tready && pix_two_ok)begin// modülden pixel çıktı.Şimdi karşılaştır.
      if(scbpacket.m_axis_tdata != expected_pix_two)begin
        $display("Data Err..Input=%0x Output=%0x Expected=%0x \n",pix_two,scbpacket.m_axis_tdata,expected_pix_two);
        dataerr = 1'b1;
        $finish();
      end
      if(scbpacket.m_axis_tlast != pix_two_tlast)begin
        $display("Tlast Err... Input=%0x Output=%0x Expected=%0x \n",pix_two_tlast,scbpacket.m_axis_tlast,pix_two_tlast);
        tlasterr = 1'b1;
        $finish();
      end
      if(scbpacket.m_axis_tuser != pix_two_tuser)begin
        $display("Tuser Err... Input=%0x Output=%0x Expected=%0x \n",pix_two_tuser,scbpacket.m_axis_tuser,pix_two_tuser);
        tusererr=1'b1;
        $finish();
      end
      if(dataerr | tlasterr | tusererr )begin
        $display("ERR.Abort... \n");
        $finish();
      end
      $display("Input=%0x output=%0x expected=%0x ------ OK ",pix_two,scbpacket.m_axis_tdata,expected_pix_two);
      pix_two_ok = 1'b0;
    end
    
    
  endtask
 
  
endclass