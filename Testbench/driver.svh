class mydriver extends uvm_driver#(mytransaction);
  
  `uvm_component_utils(mydriver)
  
  mytransaction drvpacket;
  virtual axi_stream drvif;
  
  function new(string name="mydriver",uvm_component parent=null);
    super.new(name,parent);
  endfunction
  
  function void build_phase(uvm_phase phase);
    super.build_phase(phase);
    drvpacket=mytransaction::type_id::create("drvpacket");
    uvm_config_db#(virtual axi_stream)::get(this,"tbtop","tbif",drvif);
  endfunction
  
  task run_phase(uvm_phase phase);
    forever begin
      seq_item_port.get_next_item(drvpacket);
      fork 
        send_data(drvpacket);
      join
      seq_item_port.item_done();
    end
    
  endtask
  
  task send_data(mytransaction drvpacket);
    drvif.s_axis_tdata = {drvpacket.R,drvpacket.B,drvpacket.G};
    drvif.s_axis_tvalid = drvpacket.s_axis_tvalid;
    drvif.s_axis_tlast = drvpacket.s_axis_tlast;
    drvif.s_axis_tuser = drvpacket.s_axis_tuser;
    drvif.m_axis_tready = drvpacket.m_axis_tready; 
    //drvif.m_axis_tready = 1'b1;
    @(posedge drvif.axi_aclk);// module accepted the values above.
    #1; // forwarding sim to 1 ns to drive new values on to the interface.
  endtask
  
endclass