class mytransaction extends uvm_sequence_item;
  
  rand logic [7:0] R;
  rand logic [7:0] G;
  rand logic [7:0] B;
  
  logic [23:0] s_axis_tdata;
  
  rand logic s_axis_tuser;
  rand logic s_axis_tlast;
  rand logic s_axis_tready;
  rand logic s_axis_tvalid;
  //-----------------------------------------------------
  logic [23:0] m_axis_tdata;
  
  logic 	   m_axis_tuser;
  logic 	   m_axis_tlast;
  logic 	   m_axis_tvalid;
  rand logic 	   m_axis_tready;
  
  //------------------------------------------------------
  
  //constraint tvalid_c {s_axis_tvalid == 1'b1;}
  
  `uvm_object_utils_begin(mytransaction)
  `uvm_field_int(R,UVM_DEFAULT)
  `uvm_field_int(G,UVM_DEFAULT)
  `uvm_field_int(B,UVM_DEFAULT)
  `uvm_field_int(s_axis_tdata,UVM_DEFAULT)
  `uvm_field_int(s_axis_tuser,UVM_DEFAULT)
  `uvm_field_int(s_axis_tlast,UVM_DEFAULT)
  `uvm_field_int(s_axis_tready,UVM_DEFAULT)
  `uvm_field_int(s_axis_tvalid,UVM_DEFAULT)
  // testbench input signals
  `uvm_field_int(m_axis_tdata,UVM_DEFAULT)
  `uvm_field_int(m_axis_tuser,UVM_DEFAULT)
  `uvm_field_int(m_axis_tlast,UVM_DEFAULT)
  `uvm_field_int(m_axis_tready,UVM_DEFAULT)
  `uvm_field_int(m_axis_tvalid,UVM_DEFAULT)
  `uvm_object_utils_end
  
  
  function new(string name="mytransaction");
    super.new(name);
  endfunction
  
  
endclass