interface axi_stream;
  // global signals
  logic axi_aclk;
  logic axi_arstn;
  
  // testbench master
  logic [23:0] s_axis_tdata;
  logic        s_axis_tvalid;
  logic        s_axis_tready;
  logic        s_axis_tlast;
  logic        s_axis_tuser;
  
  // testbench slave signals
  logic [23:0] m_axis_tdata;
  logic        m_axis_tvalid;
  logic        m_axis_tready;
  logic        m_axis_tlast;
  logic        m_axis_tuser;
  
endinterface


module Rgb2Gray_V2(
// global signals.This module will work at 100mhz
input wire         axi_aclk,
input wire         axi_arstn,
// slave side signals
input  wire [23:0] s_axis_tdata,
input  wire        s_axis_tvalid,
output wire        s_axis_tready,
input  wire        s_axis_tlast,
input  wire        s_axis_tuser,
// master side signals
output wire [23:0] m_axis_tdata,
output wire        m_axis_tvalid,
input  wire        m_axis_tready,
output wire        m_axis_tlast,
output wire        m_axis_tuser
    );

reg [26:0] pix_one; // data,valid,last,user
reg [26:0] pix_two; // data,valid,last,user

reg [23:0] pix_one_reg = 24'b0;
reg [23:0] pix_two_reg = 24'b0;

reg [7:0] red_one=8'b0;
reg [7:0] green_one=8'b0;
reg [7:0] blue_one=8'b0;

reg pix_one_ok=1'b0;
reg pix_two_ok=1'b0;

parameter GET_IDLE=2'd0,PIX_ONE=2'd1,PIX_TWO=2'd2;
reg [1:0] get_state=GET_IDLE;  

parameter SET_IDLE=2'd0,SEND_PIX_ONE=2'd1,SEND_PIX_TWO=2'd2;
reg [1:0] set_state=SET_IDLE;

assign s_axis_tready = m_axis_tready;

assign m_axis_tdata  = (pix_one_ok)?{pix_one[10:3],pix_one[10:3],pix_one[10:3]}:( (pix_two_ok)?{pix_two[10:3],pix_two[10:3],pix_two[10:3]}:24'b0 );
assign m_axis_tvalid = (pix_one_ok | pix_two_ok)?1'b1:1'b0;
assign m_axis_tlast  = (pix_one_ok)?pix_one[1]:( (pix_two_ok)?pix_two[1]:1'b0 );
assign m_axis_tuser  = (pix_one_ok)?pix_one[0]:( (pix_two_ok)?pix_two[0]:1'b0 );
 
always @(posedge axi_aclk)begin
    if(!axi_arstn)begin
        get_state <= GET_IDLE;
        set_state <= SEND_PIX_ONE;
        pix_one_ok <= 1'b0;
        pix_two_ok <= 1'b0;
        pix_one <= 27'b0;
        pix_two <= 27'b0;
    end
    
    else begin
        case(get_state)
            GET_IDLE:begin
               if(s_axis_tvalid && s_axis_tready)begin
                  get_state  <= PIX_ONE;
                  pix_one    <= {16'b0,((s_axis_tdata[23:16]>>2) + (s_axis_tdata[23:16]>>5) + (s_axis_tdata[15:8]>>1) + (s_axis_tdata[15:8]>>4) + (s_axis_tdata[7:0]>>4) + (s_axis_tdata[7:0]>>5)),s_axis_tvalid,s_axis_tlast,s_axis_tuser};
                  pix_one_reg <= (s_axis_tdata[23:16]>>2 + s_axis_tdata[23:16]>>5 + s_axis_tdata[15:8]>>1 + s_axis_tdata[15:8]>>4 + s_axis_tdata[7:0]>>4 + s_axis_tdata[7:0]>>5);
                  red_one   <= (s_axis_tdata[23:16]>>2) + (s_axis_tdata[23:16]>>5);
                  blue_one  <= (s_axis_tdata[15:8]>>1) + (s_axis_tdata[15:8]>>4) ;
                  green_one <=  (s_axis_tdata[7:0]>>4) + (s_axis_tdata[7:0]>>5);
                  pix_one_ok <= 1'b1;
               end
               else begin
                 get_state  <= GET_IDLE;  
               end 
            end
            
            PIX_ONE:begin
               if(s_axis_tvalid && s_axis_tready)begin
                 get_state  <= PIX_TWO;
                 pix_two    <= {16'b0,((s_axis_tdata[23:16]>>2) + (s_axis_tdata[23:16]>>5) + (s_axis_tdata[15:8]>>1) + (s_axis_tdata[15:8]>>4) + (s_axis_tdata[7:0]>>4) + (s_axis_tdata[7:0]>>5)),s_axis_tvalid,s_axis_tlast,s_axis_tuser};
                 pix_two_reg <= (s_axis_tdata[23:16]>>2 + s_axis_tdata[23:16]>>5 + s_axis_tdata[15:8]>>1 + s_axis_tdata[15:8]>>4 + s_axis_tdata[7:0]>>4 + s_axis_tdata[7:0]>>5);
                 pix_two_ok <= 1'b1; 
               end
               else begin
                 get_state <= PIX_ONE;
               end
            end
            
            PIX_TWO:begin
               if(s_axis_tvalid && s_axis_tready)begin
                 get_state  <= PIX_ONE;
                 pix_one    <= {16'b0,((s_axis_tdata[23:16]>>2) + (s_axis_tdata[23:16]>>5) + (s_axis_tdata[15:8]>>1) + (s_axis_tdata[15:8]>>4) + (s_axis_tdata[7:0]>>4) + (s_axis_tdata[7:0]>>5)),s_axis_tvalid,s_axis_tlast,s_axis_tuser};
                 pix_one_reg <= (s_axis_tdata[23:16]>>2 + s_axis_tdata[23:16]>>5 + s_axis_tdata[15:8]>>1 + s_axis_tdata[15:8]>>4 + s_axis_tdata[7:0]>>4 + s_axis_tdata[7:0]>>5);
                 pix_one_ok <= 1'b1;
               end
               else begin
                 get_state <= PIX_TWO;
               end
            end
            default:begin
               get_state <= GET_IDLE; 
            end
        endcase
       
        case(set_state)
            SEND_PIX_ONE:begin
                if(m_axis_tready && m_axis_tvalid && pix_one_ok)begin
                  set_state  <= SEND_PIX_TWO;
                  pix_one_ok <= 1'b0;
                end
                else begin
                  set_state <= set_state;
                end
            end
            SEND_PIX_TWO:begin
                if(m_axis_tready && m_axis_tvalid && pix_two_ok)begin
                    set_state <= SEND_PIX_ONE;
                    pix_two_ok <= 1'b0;
                end
                else begin
                    set_state <= set_state;
                end
            end
            default:begin
                set_state <= SEND_PIX_ONE;
            end
        endcase    
    end


end

    
endmodule